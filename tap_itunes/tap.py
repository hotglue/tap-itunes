from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_itunes.streams import CustomerReviewsStream

STREAM_TYPES = [CustomerReviewsStream]


class TapiTunes(Tap):
    """iTunes tap class."""

    name = "tap-itunes"

    config_jsonschema = th.PropertiesList(
        th.Property("app_id", th.StringType),
        th.Property("start_date", th.DateTimeType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapiTunes.cli()
