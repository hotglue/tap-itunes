"""REST client handling, including iTunesStream base class."""

from datetime import date, datetime
from typing import Any, Dict, Iterable, List, Optional, Union
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
import requests
from singer.schema import Schema
from singer_sdk.plugin_base import PluginBase as TapBaseClass
from singer_sdk.streams import RESTStream

from memoization import cached
from pendulum import parse

class iTunesStream(RESTStream):

    url_base = "https://itunes.apple.com"

    page = 1
    updated = None
    last_page = False

    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        if self.last_page:
            self.last_page = False
            self.page = 1
            return None
        
        if not response.json()["feed"].get("entry") or previous_token==10:
            self.page = 1
            return None

        next_page = (previous_token or 1) + 1
        self.page = next_page
        return next_page

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        updated = parse(row[self.replication_key]["label"])
        start_date = self.get_starting_time(context)
        if updated <= start_date:
            self.last_page = True
            return None
        for field in row.keys():
            if field == "author":
                row[field] = row[field]["name"]["label"]
            if field in [
                        "updated",
                        "im:rating",
                        "im:version",
                        "id",
                        "title",
                        "im:voteSum",
                        "im:voteCount",
                        "content"
                        ]:
                row[field] = row[field]["label"]
        return row

    def validate_response(self, response: requests.Response) -> None:
       
        if response.status_code == 403 :
            return 
        
        elif (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        
        if  response.status_code == 403 :
            return []  
        else: 
             yield from extract_jsonpath(self.records_jsonpath, input=response.json())